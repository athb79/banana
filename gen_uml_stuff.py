import sys
import clang.cindex
import pprint
import os
import re

decls = {}
decls[''] = {}
vars = {}
vars[''] = {}

def callexpr_visitor(cursor, scope=''):
    if '/usr/include' in str(cursor.location.file):
        return
    if cursor.kind == clang.cindex.CursorKind.CLASS_TEMPLATE or cursor.kind == clang.cindex.CursorKind.STRUCT_DECL or cursor.kind == clang.cindex.CursorKind.CLASS_DECL:
        if scope != '':
           scope += '::' + cursor.spelling
        else:
           scope = cursor.spelling
        decls[scope] = {}
        vars[scope] = {}
    elif cursor.kind == clang.cindex.CursorKind.CONSTRUCTOR or cursor.kind == clang.cindex.CursorKind.DESTRUCTOR:
        if "__" not in cursor.spelling:
            adder = ''
            if cursor.kind == clang.cindex.CursorKind.DESTRUCTOR:
                adder = '~'
            try:
                decls[scope][adder + cursor.semantic_parent.spelling]
            except:
                decls[scope][adder + cursor.semantic_parent.spelling] = []
            to_add = decls[scope][adder + cursor.semantic_parent.spelling]
            to_add.append({})
            to_add[-1]["args"] = []
            to_add[-1]["access"] = cursor.access_specifier
            for thing in cursor.get_arguments():
                to_add[-1]["args"].append({})
                to_add[-1]["args"][-1]["type"] = thing.type.spelling
                to_add[-1]["args"][-1]["name"] = thing.spelling
                #print('\t{} {}'.format(thing.type.spelling, thing.spelling))
    elif cursor.kind == clang.cindex.CursorKind.CXX_METHOD or cursor.kind == clang.cindex.CursorKind.FUNCTION_TEMPLATE or cursor.kind == clang.cindex.CursorKind.FUNCTION_DECL:
        #print('{}'.format(cursor.spelling))
        if "__" not in cursor.spelling:
            #print('class method: {} {}'.format(cursor.result_type.spelling, cursor.spelling))
            try:
                decls[scope][cursor.spelling]
            except:
                decls[scope][cursor.spelling] = []
            add_to = decls[scope][cursor.spelling]
            add_to.append({})
            add_to[-1]["args"] = []
            add_to[-1]["access"] = cursor.access_specifier
            add_to[-1]["result"] = cursor.result_type.spelling
            add_to[-1]["const"] = (cursor.get_usr()[-1] == '1')
            for thing in cursor.get_arguments():
                add_to[-1]["args"].append({})
                add_to[-1]["args"][-1]["type"] = thing.type.spelling
                add_to[-1]["args"][-1]["name"] = thing.spelling
                #print('\t{} {}'.format(thing.type.spelling, thing.spelling))
            #this needs to be special for some stupid reason :/
            #I don't know how to deal with this so make force a workaround
            if cursor.kind == clang.cindex.CursorKind.FUNCTION_TEMPLATE:
                print("Error: Global template functions are not allowed.")
                print("       [This is due to what I must assume is a bug in get_arguments()]")
                print("       Remove template parameters and use a using alias (e.g. using T = int)")
                sys.exit(12345)
    elif cursor.kind == clang.cindex.CursorKind.NAMESPACE:
        if cursor.spelling in ("std", "__gnu_cxx", "__exception_ptr", "__gnu_debug", "__debug", "__gnu_cxx", "rel_ops", "__cxxabiv1"):
            return
        if scope:
            scope += '::' + cursor.spelling
        else:
            scope = cursor.spelling
    elif cursor.kind == clang.cindex.CursorKind.FIELD_DECL:
        vars[scope][cursor.spelling] = {}
        add_to = vars[scope][cursor.spelling]
        add_to["type"] = cursor.type.spelling
        add_to["access"] = cursor.access_specifier
    for child in cursor.get_children():
        callexpr_visitor(child, scope)

index = clang.cindex.Index.create()
#define a bunch of included things to prevent undefined things
tu = index.parse(sys.argv[1],
                args=['-x',
                      'c++',
                      '-std=c++11',
                      '-D_IMMINTRIN_H_INCLUDED',
                      '-D_XMMINTRIN_H_INCLUDED',
                      '-D_EMMINTRIN_H_INCLUDED',
                      '-D_PMMINTRIN_H_INCLUDED',
                      '-D_TMMINTRIN_H_INCLUDED',
                      '-D_SMMINTRIN_H_INCLUDED',
                      '-D_WMMINTRIN_H_INCLUDED',
                      '-D_X86INTRIN_H_INCLUDED'
                      ])

conver = {}
conver[clang.cindex.AccessSpecifier.PUBLIC] = '+'
conver[clang.cindex.AccessSpecifier.PRIVATE] = '-'
conver[clang.cindex.AccessSpecifier.PROTECTED] = '#'
#global func
conver[clang.cindex.AccessSpecifier.INVALID] = '+'

def getinout(string):
    if '&' in string and 'const' in string:
        return 'in'
    elif '&&' in string:
        return 'inout'
    elif '&' in string:
        return '{$PUT inout OR out}'
    else:
        return 'in'

if len(tu.diagnostics) > 0:
    pprint.pprint(list(tu.diagnostics))
else:
    callexpr_visitor(tu.cursor)
    for key, item in decls.iteritems():
        print("\n---------------------\n{}\n---------------------\n".format(key))
        print('{')
        for name, var in vars[key].iteritems():
            to_print = conver[var['access']] + ' ' + name + ' : ' + var['type'] + '\\\\'
            to_print = to_print.replace('&', '\&')
            to_print = to_print.replace('_','\_')
            print(to_print)
        print('}\n{')
        for function, infos in sorted(item.iteritems()):
            for info in infos:
                to_print = conver[info['access']] + ' '
                to_print += function + '('
                blah = ''
                for arg in info['args']:
                    to_print += blah + getinout(arg['type']) + ' ' + arg['name'] + ' : ' + arg['type']
                    blah = ', '
                try:
                    to_print += ') : ' + info['result'] + '\\\\'
                except:
                    #constructor or destructor
                    to_print += ')\\\\'
            #formating
            to_print = to_print.replace('&', '\&')
            to_print = to_print.replace('<<', '<\hspace{0pt}<')
            to_print = to_print.replace('>>', '>\hspace{0pt}>')
            to_print = to_print.replace('~','$\sim$')
            to_print = to_print.replace('_','\_')
            to_print = to_print.replace('-','--')
            print(to_print)
        print('}')
